package personal.rowan.paperforflickr.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import org.scribe.model.Token;

import personal.rowan.paperforflickr.R;
import personal.rowan.paperforflickr.request.AccessTokenRequest;
import personal.rowan.paperforflickr.request.RequestTokenRequest;

/**
 * Created by Rowan on 9/4/15
 */
public class AuthenticationFragment extends BaseFragment{

    private static final String FRAG_TAG = "Authenticate with Flickr";

    private EditText etAuthorization;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_authentication, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final WebView webView = (WebView) view.findViewById(R.id.frag_authentication_wv);
        etAuthorization = (EditText) view.findViewById(R.id.frag_authentication_et);
        Button btnAuthorization = (Button) view.findViewById(R.id.frag_authentication_btn);

        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        final RequestTokenRequest requestTokenRequest = new RequestTokenRequest();
        final Token[] requestToken = new Token[1];
        requestTokenRequest.requestPostExecute = new RequestTokenRequest.IRequestPostExecute() {
            @Override
            public void onPostExecute() {
                Pair<Token, String> response = requestTokenRequest.getResult();
                requestToken[0] = response.first;
                webView.loadUrl(response.second);
            }
        };
        requestTokenRequest.execute();

        btnAuthorization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestToken[0] != null && !etAuthorization.getText().toString().isEmpty()) {
                    final AccessTokenRequest accessTokenRequest = new AccessTokenRequest();
                    accessTokenRequest.requestPostExecute = new AccessTokenRequest.IRequestPostExecute() {
                        @Override
                        public void onPostExecute() {
                            activity.accessToken = accessTokenRequest.getResult();
                            activity.saveToken();
                            closeSoftwareKeyboard();
                            navigateToFragment(new LocationPhotosFragment(), false, false, null);
                        }
                    };
                    //noinspection unchecked
                    accessTokenRequest.execute(new Pair<>(requestToken[0], etAuthorization.getText().toString()));
                }
            }
        });
    }

    @Override
    public String fragmentTag() {
        return FRAG_TAG;
    }

    @Override
    public boolean setActionBarTitle() {
        return false;
    }

    private void closeSoftwareKeyboard() {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etAuthorization.getWindowToken(), 0);
    }
}
