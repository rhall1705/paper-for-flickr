package personal.rowan.paperforflickr.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import personal.rowan.paperforflickr.R;
import personal.rowan.paperforflickr.request.VolleySingleton;

/**
 * Created by Rowan on 9/9/15
 */
public class PhotoDisplayFragment extends BaseFragment{

    private final static String FRAG_TAG = "Photos Display";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_photo_display, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AnimatedNetworkImageView ivPhotoDisplay = (AnimatedNetworkImageView) view.findViewById(R.id.frag_photo_display_page_iv);
        Bundle args = getArguments();
        if(args != null) {
            String imageUrl = args.getString(LocationPhotoAdapter.ARGS_PHOTO_DISPLAY);
            ivPhotoDisplay.setImageUrl(imageUrl, VolleySingleton.getInstance().getImageLoader());
        }
        FrameLayout flPhotoDisplay = (FrameLayout) view.findViewById(R.id.frag_photo_display_page_fl);
        flPhotoDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });

    }

    @Override
    public String fragmentTag() {
        return FRAG_TAG;
    }

    @Override
    public boolean setActionBarTitle() {
        return false;
    }

}
