package personal.rowan.paperforflickr.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import personal.rowan.paperforflickr.model.Photo;
import personal.rowan.paperforflickr.R;
import personal.rowan.paperforflickr.request.BaseScribeRequest;
import personal.rowan.paperforflickr.request.PhotosForLocationRequest;

/**
 * Created by Rowan on 9/4/15
 */
public class LocationPhotosFragment extends BaseFragment{

    private static final String FRAG_TAG = "Paper for flickr";

    private List<Photo> locationPhotos;

    private SwipeRefreshLayout srlPhotos;
    private LinearLayout llProgressContainer;
    private LocationPhotoAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.frag_locationphotos, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.frag_locationphotos_toolbar);
        activity.setSupportActionBar(toolbar);
        RecyclerView rvPhotos = (RecyclerView) view.findViewById(R.id.frag_locationphotos_rv);
        int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        boolean tablet = screenSize != Configuration.SCREENLAYOUT_SIZE_SMALL && screenSize != Configuration.SCREENLAYOUT_SIZE_NORMAL;
        rvPhotos.setLayoutManager(!tablet ? new LinearLayoutManager(activity) : new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        llProgressContainer = (LinearLayout) view.findViewById(R.id.frag_locationphotos_progress_container_ll);
        srlPhotos = (SwipeRefreshLayout) view.findViewById(R.id.frag_locationphotos_srl);
        srlPhotos.setColorSchemeResources(R.color.ColorPrimary, R.color.ColorAccent);
        srlPhotos.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPhotos();
            }
        });

        locationPhotos = activity.loadPhotos();
        rvPhotos.setAdapter(adapter = new LocationPhotoAdapter(locationPhotos, this));

        if(savedInstanceState == null) {
            llProgressContainer.setVisibility(View.VISIBLE);
            loadPhotos();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.frag_locationphotos_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.frag_locationphotos_menu_login:
                navigateToFragment(new AuthenticationFragment(), true, false, null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadPhotos() {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        final PhotosForLocationRequest photosForLocationRequest = new PhotosForLocationRequest();
        photosForLocationRequest.requestPostExecute = new BaseScribeRequest.IRequestPostExecute() {
            @Override
            public void onPostExecute() {
                llProgressContainer.setVisibility(View.GONE);
                srlPhotos.setRefreshing(false);
                locationPhotos = photosForLocationRequest.getResult();
                if(locationPhotos != null && !locationPhotos.isEmpty()) {
                    adapter.setPhotos(locationPhotos);
                    adapter.notifyDataSetChanged();
                    activity.savePhotos(locationPhotos);
                }
            }
        };
        //noinspection unchecked
        photosForLocationRequest.execute(new Pair<>(activity.accessToken, new Double[]{lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()}));
    }

    @Override
    public String fragmentTag() {
        return FRAG_TAG;
    }
}
