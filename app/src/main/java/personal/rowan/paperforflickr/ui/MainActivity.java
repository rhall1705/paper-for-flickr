package personal.rowan.paperforflickr.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import org.scribe.model.Token;

import java.util.ArrayList;
import java.util.List;

import personal.rowan.paperforflickr.model.Photo;
import personal.rowan.paperforflickr.R;
import personal.rowan.paperforflickr.request.BaseScribeRequest;
import personal.rowan.paperforflickr.request.CheckTokenRequest;


public class MainActivity extends AppCompatActivity {

    public Token accessToken;
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;

    private FrameLayout flMainContainer;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        fragmentManager = getSupportFragmentManager();
        sharedPreferences = getSharedPreferences("com.personal.rowan.paperforflickr", Context.MODE_PRIVATE);
        flMainContainer = (FrameLayout) findViewById(R.id.act_main_container_fl);

        if (savedInstanceState == null) {
            handleTokens();
            navigateToFragment(new LocationPhotosFragment(), false, false, null);
        }

    }

    public void navigateToFragment(BaseFragment fragment, boolean addToBackStack, boolean add, Bundle args) {
        if (fragmentManager.findFragmentById(flMainContainer.getId()) == null ||
                !fragmentManager.findFragmentById(flMainContainer.getId()).getTag().equals(fragment.fragmentTag())) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (addToBackStack) {
                transaction.addToBackStack(fragment.fragmentTag());
            }
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
            if (args != null) {
                fragment.setArguments(args);
            }
            if (add) {
                transaction.add(flMainContainer.getId(), fragment, fragment.fragmentTag());
            } else {
                transaction.replace(flMainContainer.getId(), fragment, fragment.fragmentTag());
            }
            transaction.commit();
        }
    }

    private void handleTokens() {
        accessToken = loadToken();
        if(accessToken != null) {
            final CheckTokenRequest checkTokenRequest = new CheckTokenRequest();
            checkTokenRequest.requestPostExecute = new BaseScribeRequest.IRequestPostExecute() {
                @Override
                public void onPostExecute() {
                    boolean success = checkTokenRequest.getResult();
                    Snackbar snackbar = Snackbar.make(flMainContainer, success ?
                            R.string.act_main_logged_in : R.string.act_main_login_expired, Snackbar.LENGTH_LONG);
                    if(!success) {
                        snackbar.setAction("Login", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                navigateToFragment(new AuthenticationFragment(), true, false, null);
                            }
                        });
                    }
                    snackbar.show();
                }
            };
            checkTokenRequest.execute(accessToken);
        }
    }


    public void saveToken() {
        if(accessToken != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("access_token", accessToken.getToken());
            editor.putString("access_token_secret", accessToken.getSecret());
            editor.apply();
        }
    }

    public Token loadToken() {
        String token = sharedPreferences.getString("access_token", null);
        String secret = sharedPreferences.getString("access_token_secret", null);
        return token != null && secret != null ? new Token(token, secret) : null;
    }

    public void savePhotos(List<Photo> photos) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("numPhotos", photos.size());
        for(int i = 0; i < photos.size(); i++) {
            Photo photo = photos.get(i);
            editor.putString("title_" + String.valueOf(i), photo.getTitle());
            editor.putString("url_medium_" + String.valueOf(i), photo.getMediumPhotoUrl());
            editor.putString("url_large_" + String.valueOf(i), photo.getLargePhotoUrl());
        }
        editor.apply();
    }

    public List<Photo> loadPhotos() {
        List<Photo> photos = new ArrayList<>();
        int numPosts = sharedPreferences.getInt("numPhotos", 0);

        for (int i = 0; i < numPosts; i++) {
            String title = sharedPreferences.getString("title_" + String.valueOf(i), null);
            String urlMedium = sharedPreferences.getString("url_medium_" + String.valueOf(i), null);
            String urlLarge = sharedPreferences.getString("url_large_" + String.valueOf(i), null);
            photos.add(new Photo(title, urlMedium, urlLarge));
        }

        return photos;
    }

}
