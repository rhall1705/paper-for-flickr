package personal.rowan.paperforflickr.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.Window;
import android.view.WindowManager;

import personal.rowan.paperforflickr.R;

/**
 * Created by Rowan on 9/1/15
 */
public abstract class BaseFragment extends Fragment {

    protected MainActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar != null) {
            if (setActionBarTitle()) {
                actionBar.setTitle(fragmentTag());
            }
        }
        if(setStatusBarColor() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //noinspection deprecation
            window.setStatusBarColor(getResources().getColor(R.color.ColorPrimaryDark));
        }
    }

    public void navigateToFragment(BaseFragment fragment, boolean addToBackStack, boolean add, Bundle args) {
        activity.navigateToFragment(fragment, addToBackStack, add, args);
    }

    public boolean setActionBarTitle() {
        return true;
    }

    public boolean setStatusBarColor() { return true; }

    public abstract String fragmentTag();
}
