package personal.rowan.paperforflickr.ui;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import personal.rowan.paperforflickr.model.Photo;
import personal.rowan.paperforflickr.R;
import personal.rowan.paperforflickr.request.VolleySingleton;

/**
 * Created by Rowan on 9/9/15
 */
public class LocationPhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String ARGS_PHOTO_DISPLAY = "args_photo_display";

    private List<Photo> photos;
    private BaseFragment fragment;
    private int lastPosition = -1;

    public LocationPhotoAdapter(List<Photo> photos, BaseFragment fragment) {
        this.photos = photos;
        this.fragment = fragment;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    //Create custom viewholder in case we eventually want other types of posts
    private class PhotoViewHolder extends RecyclerView.ViewHolder {

        public CardView cvPhoto;
        public TextView tvTitle;
        public NetworkImageView ivPhoto;
        public FrameLayout flClickable;

        public PhotoViewHolder(View v) {
            super(v);
            cvPhoto = (CardView) v.findViewById(R.id.view_photo_card_cv);
            tvTitle = (TextView) v.findViewById(R.id.view_photo_card_title_tv);
            ivPhoto = (NetworkImageView) v.findViewById(R.id.view_photo_card_iv);
            flClickable = (FrameLayout) v.findViewById(R.id.view_photo_card_clickable_fl);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_photo_card, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Photo photo = photos.get(position);
        PhotoViewHolder photoViewHolder = (PhotoViewHolder) holder;
        setAnimation(photoViewHolder.cvPhoto, position);
        photoViewHolder.tvTitle.setText(photo.getTitle());
        photoViewHolder.ivPhoto.setImageUrl(photo.getMediumPhotoUrl(), VolleySingleton.getInstance().getImageLoader());
        photoViewHolder.flClickable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString(ARGS_PHOTO_DISPLAY, photo.getLargePhotoUrl());
                fragment.navigateToFragment(new PhotoDisplayFragment(), true, true, args);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(fragment.activity, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
