package personal.rowan.paperforflickr.request;

import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FlickrApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import personal.rowan.paperforflickr.model.FlickrValues;

/**
 * Created by Rowan on 9/4/15
 */
public class CheckTokenRequest extends BaseScribeRequest<Token, Void, Boolean> {
    @Override
    protected Boolean doInBackground(Token[] params) {
        String PROTECTED_RESOURCE_URL = "https://api.flickr.com/services/rest/";
        OAuthService service = new ServiceBuilder().provider(FlickrApi.class).apiKey(FlickrValues.CONSUMER_KEY).apiSecret(FlickrValues.CONSUMER_SECRET).build();
        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        request.addQuerystringParameter("method", "flickr.auth.oauth.checkToken");
        request.addQuerystringParameter("format", "json");
        service.signRequest(params[0], request);
        org.scribe.model.Response response = request.send();
        boolean valid = false;
        try {
            String responseBody = removeFlickrHeader(response.getBody());
            JSONObject jsonObject = new JSONObject(responseBody);
            String status = jsonObject.getString("stat");
            valid = status.equals("ok");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return valid;
    }
}
