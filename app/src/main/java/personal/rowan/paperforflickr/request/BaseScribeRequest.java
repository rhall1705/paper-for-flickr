package personal.rowan.paperforflickr.request;

import android.os.AsyncTask;

/**
 * Created by Rowan on 9/4/15
 */
public abstract class BaseScribeRequest<Params, Void, Return> extends AsyncTask<Params, Void, Return> {

    public IRequestPostExecute requestPostExecute;
    protected Return result;

    @Override
    protected abstract Return doInBackground(Params[] params);

    @Override
    protected void onPostExecute(Return result) {
        this.result = result;
        if(requestPostExecute != null) {
            requestPostExecute.onPostExecute();
        }
    }

    public interface IRequestPostExecute {
        void onPostExecute();
    }

    public Return getResult() {
        return result;
    }

    protected String removeFlickrHeader(String responseBody) {
        responseBody = responseBody.replace("jsonFlickrApi(", "");
        return responseBody.substring(0, responseBody.length() - 1);
    }
}
