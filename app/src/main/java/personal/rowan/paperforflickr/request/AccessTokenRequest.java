package personal.rowan.paperforflickr.request;

import android.util.Log;
import android.util.Pair;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FlickrApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import personal.rowan.paperforflickr.model.FlickrValues;

/**
 * Created by Rowan on 9/4/15
 */
public class AccessTokenRequest extends BaseScribeRequest<Pair<Token, String>, Void, Token>{

    @Override
    protected final Token doInBackground(Pair<Token, String>[] params) {
        OAuthService service = new ServiceBuilder().provider(FlickrApi.class).apiKey(FlickrValues.CONSUMER_KEY).apiSecret(FlickrValues.CONSUMER_SECRET).build();
        Token requestToken = params[0].first;
        String verifierString = params[0].second;
        Verifier verifier = new Verifier(verifierString);
        Log.d("scribeLog", "Trading the Request Token for an Access Token...");
        try {
            return service.getAccessToken(requestToken, verifier);
        } catch(Exception e) {
            return null;
        }
    }
}