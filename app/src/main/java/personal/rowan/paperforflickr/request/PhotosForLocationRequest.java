package personal.rowan.paperforflickr.request;

import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FlickrApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import java.util.ArrayList;
import java.util.List;

import personal.rowan.paperforflickr.model.FlickrValues;
import personal.rowan.paperforflickr.model.Photo;

/**
 * Created by Rowan on 9/4/15
 */
public class PhotosForLocationRequest extends BaseScribeRequest<Pair<Token, Double[]>, Void, List<Photo>> {
    @Override
    protected final List<Photo> doInBackground(Pair<Token, Double[]>[] params) {
        String PROTECTED_RESOURCE_URL = "https://api.flickr.com/services/rest/";
        OAuthService service = new ServiceBuilder().provider(FlickrApi.class).apiKey(FlickrValues.CONSUMER_KEY).apiSecret(FlickrValues.CONSUMER_SECRET).build();
        OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        request.addQuerystringParameter("method", "flickr.photos.search");
        request.addQuerystringParameter("lat", String.valueOf(params[0].second[0]));
        request.addQuerystringParameter("lon", String.valueOf(params[0].second[1]));
        request.addQuerystringParameter("format", "json");
        Token accessToken = params[0].first;
        if(accessToken != null) {
            service.signRequest(accessToken, request);
        } else {
            service.signRequest(new Token("", ""), request);
        }
        org.scribe.model.Response response = request.send();
        String responseString = removeFlickrHeader(response.getBody());
        List<Photo> photoList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(responseString);
            JSONObject photosObject = jsonObject.getJSONObject("photos");
            JSONArray photoArray = photosObject.getJSONArray("photo");
            for(int i = 0; i < photoArray.length(); i++) {
                JSONObject photoObject = photoArray.getJSONObject(i);
                String title = photoObject.getString("title");
                String farm = photoObject.getString("farm");
                String server = photoObject.getString("server");
                String photoId = photoObject.getString("id");
                String secret = photoObject.getString("secret");
                String mediumPhotoUrl = FlickrValues.generateMediumPhotoUrl(farm, server, photoId, secret);
                String largePhotoUrl = FlickrValues.generateLargePhotoUrl(farm, server, photoId, secret);
                photoList.add(new Photo(title, mediumPhotoUrl, largePhotoUrl));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return photoList;
    }
}
