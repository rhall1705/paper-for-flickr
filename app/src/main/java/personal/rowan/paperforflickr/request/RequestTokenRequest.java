package personal.rowan.paperforflickr.request;

import android.util.Log;
import android.util.Pair;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FlickrApi;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import personal.rowan.paperforflickr.model.FlickrValues;

/**
 * Created by Rowan on 9/1/15
 */
public class RequestTokenRequest extends BaseScribeRequest<Void, Void, Pair<Token, String>> {

    @Override
    protected Pair<Token, String> doInBackground(Void[] params) {
        OAuthService service = new ServiceBuilder().provider(FlickrApi.class).apiKey(FlickrValues.CONSUMER_KEY).apiSecret(FlickrValues.CONSUMER_SECRET).build();

        // Obtain the Request Token
        Log.d("scribeLog", "Fetching the Request Token...");
        Token requestToken = service.getRequestToken();
        Log.d("scribeLog", "Got the Request Token! It's: " + requestToken.getToken());

        Log.d("scribeLog", "Now go and authorize Scribe here:");
        String authorizationUrl = service.getAuthorizationUrl(requestToken) + "&perms=read";
        return new Pair<>(requestToken, authorizationUrl);
    }
}
