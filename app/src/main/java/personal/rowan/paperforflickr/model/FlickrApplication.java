package personal.rowan.paperforflickr.model;

import android.app.Application;

/**
 * Created by Rowan on 9/9/15
 */
public class FlickrApplication extends Application {
    private static FlickrApplication mInstance;

    public static FlickrApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}

