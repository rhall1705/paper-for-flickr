package personal.rowan.paperforflickr.model;

/**
 * Created by Rowan on 9/9/15
 */
public class Photo {
    private String title;
    private String mediumPhotoUrl;
    private String largePhotoUrl;

    public Photo(String title, String mediumPhotoUrl, String largePhotoUrl) {
        this.title = title;
        this.mediumPhotoUrl = mediumPhotoUrl;
        this.largePhotoUrl = largePhotoUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getMediumPhotoUrl() {
        return mediumPhotoUrl;
    }

    public String getLargePhotoUrl() {
        return largePhotoUrl;
    }
}
