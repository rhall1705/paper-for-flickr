package personal.rowan.paperforflickr.model;

import android.text.TextUtils;

/**
 * Created by Rowan on 9/1/15
 */
public class FlickrValues {

    public static final String CONSUMER_KEY = "37b675f10fcc8e8d7dc5ce1cdf547e5a";
    public static final String CONSUMER_SECRET = "76bb3e0937c95a37";

    public static String generateThumbnailPhotoUrl(String farm, String server, String photoId,
                                                         String secret) {
        return generatePhotoUrl("t", farm, server, photoId, secret);
    }

    public static String generateSmallPhotoUrl(String farm, String server, String photoId,
                                                     String secret) {
        return generatePhotoUrl("n", farm, server, photoId, secret);
    }

    public static String generateMediumPhotoUrl(String farm, String server, String photoId,
                                                      String secret) {
        return generatePhotoUrl("z", farm, server, photoId, secret);
    }

    public static String generateLargePhotoUrl(String farm, String server, String photoId,
                                                     String secret) {
        return generatePhotoUrl("b", farm, server, photoId, secret);
    }

    private static String generatePhotoUrl(String suffix, String farm, String server, String photoId,
                                           String secret) {
        return String.format("http://farm%s.staticflickr.com/%s/%s_%s_%s.jpg", farm, server,
                photoId, secret, suffix);
    }

    public static String generateBuddyIcon(int iconFarm, String iconServer, String nsid) {
        if (TextUtils.isEmpty(nsid) || TextUtils.isEmpty(iconServer)
                || TextUtils.equals(iconServer, "0")) {
            return "http://www.flickr.com/images/buddyicon.gif";
        } else {
            return String.format("http://farm%s.staticflickr.com/%s/buddyicons/%s.jpg", iconFarm,
                    iconServer, nsid);
        }
    }

    private FlickrValues() {
    }

}
