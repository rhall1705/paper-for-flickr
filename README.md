An Android application used to access Flickr images drawn from the user's current/most recently measured location. Features include:


Separate phone/tablet layout

Login to flickr to see non-public photos

Photos/login info kept persistent/cached for convenience and offline viewing

Material design - cards, ripples, snackbars, etc.

Animations - cards slide in, images fade in as they load, toolbar hides/emerges on scroll

Full screen view - tap a card's image to see its full size

Swipe to refresh


Set up project by pulling to Android Studio and running on an Android device API 16+